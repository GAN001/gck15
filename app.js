//ARCHITECTURE D'UNE APPLICATION WEB
const log = console.log;
const path = require('path');
const express = require('express'); 
const { engine }= require('express-handlebars');
const app = express();
const port = 3000;

app.engine('handlebars', engine());
app.set('view engine', 'handlebars');
//app.set('views', path.join(__dirname, 'views'));

app.use(express.static(path.join(__dirname, "public")));

app.get('/', (req,res) => {
    res.render('home', {
        title: 'Home',
        age: 69
    });
});

app.get('/about', (req,res) => {
    res.render('about', {
        title: 'About'
    });
});


app.listen(port, log(`Server listening at port ${port}`));
 
